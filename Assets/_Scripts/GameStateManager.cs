﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameStateManager : MonoBehaviour
{
	public GameState CurState = GameState.Lobby;

	[SerializeField] AudioClip possessionClip;
	[SerializeField] AudioClip deathClip;

	[SerializeField] TMP_Text screenMessage;

	int counterToWin = 0;
	int curIdx;
	bool isDisplayed;

	List<PossesableObject> objects = new List<PossesableObject>();

	void Start()
	{
		screenMessage.text = "Press any button to start!";
	}
	void Update()
	{
		switch (CurState)
		{
			case GameState.Lobby:
				// disable input except for one button / press any button to play
				Lobby();
				break;
			case GameState.GamePlay:
				// enable the default gameplay input
				break;
			case GameState.Win:
				// go into a win screen
				WinGame();
				break;
			case GameState.Lose:
				// go into a lose screen
				LoseGame();
				break;
			default:
				break;
		}
	}
	void Lobby()
	{
		if (Input.anyKey)
		{
			// start the game
			screenMessage.text = "";
			StartGame();
		}
	}
	void StartGame()
	{
		// get all possesable objects
		PossesableObject[] objArray = transform.GetComponentsInChildren<PossesableObject>();
		for (int i = 0; i < objArray.Length; i++)
		{
			objects.Add(objArray[i]);
			objects[i].Setup(this);
		}
		ChooseNextPossessedObject();

		CurState = GameState.GamePlay;
	}
	void WinGame()
	{
		// disable inputs and show screen or message
		if (!isDisplayed)
		{
			screenMessage.text = "Congratulations, you won! Press any button to reload...";
			isDisplayed = true;
		}
		else if (Input.anyKey)
		{
			SceneManager.LoadScene("SampleScene");
		}
	}
	void LoseGame()
	{
		// disable inputs and show screen or message
		if (!isDisplayed)
		{
			screenMessage.text = "You have died! Press any button to reload...";
			isDisplayed = true;
		}
		else if (Input.anyKey)
		{
			SceneManager.LoadScene("SampleScene");
		}
	}
	public void AddToCounter()
	{
		RemoveObject(curIdx);

		counterToWin++;
		if (counterToWin == 5)
		{
			CurState = GameState.Win;
		}
		else
		{
			ChooseNextPossessedObject();
		}
	}
	void ChooseNextPossessedObject()
	{
		// 100 / time to go max
		float waitTime = Random.Range(2f, 3f);
		StartCoroutine(ChooseObject(waitTime));
	}
	void RemoveObject(int idx)
	{
		objects.Remove(objects[idx]);
	}

	IEnumerator ChooseObject(float waitTime)
	{
		yield return new WaitForSeconds(waitTime);

		float factor = 100f / Random.Range(7f, 9f);

		int idx = Random.Range(0, objects.Count);
		objects[idx].StartPossession(possessionClip, deathClip, factor, idx);
	}
}

public enum GameState { Lobby, GamePlay, Win, Lose }