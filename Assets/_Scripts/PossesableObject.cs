﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PossesableObject : MonoBehaviour
{
	AudioSource source;
	AudioClip deathClip;
	GameStateManager manager;

	Vector3 offset;
	Vector3 screenPoint;

	int idx;
	[SerializeField] bool isPossessed;

	#region MonoBehaviour
	void Start()
	{
		source = GetComponent<AudioSource>();
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Box"))
		{

			if (isPossessed)
			{
				// set +1 to the counter or get some other progression
				StopAllCoroutines();
				source.Stop();
				source.clip = deathClip;
				source.Play();

				manager.AddToCounter();
			}
			else if (!isPossessed)
			{
				manager.CurState = GameState.Lose;
			}
		}
	}
	void OnMouseDown()
	{
		// grab the object
		screenPoint = Camera.main.WorldToScreenPoint(transform.position);

		offset = transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

		GetComponent<Rigidbody>().useGravity = false;
	}
	void OnMouseDrag()
	{
		// hold the object
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);

		Vector3 curPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		transform.position = curPosition;
	}
	void OnMouseUp()
	{
		GetComponent<Rigidbody>().useGravity = true;
	}
	#endregion

	#region
	public void Setup(GameStateManager _manager)
	{
		manager = _manager;
	}
	public void StartPossession(AudioClip clip, AudioClip _deathClip, float factor, int _idx)
	{
		idx = _idx;
		deathClip = _deathClip;
		GetComponent<MeshRenderer>().material.color = Color.red;
		isPossessed = true;

		source.clip = clip;
		source.volume = 0f;
		source.Play();

		StartCoroutine(GetLouder(factor));
	}
	public void StopSound()
	{
		source.Stop();
		StopAllCoroutines();
	}
	#endregion

	IEnumerator GetLouder(float factor)
	{
		while (source.volume < 0.99f)
		{
			source.volume += Time.deltaTime * factor / 100f;
			yield return null;
		}

		source.Stop();
		manager.CurState = GameState.Lose;
	}
}
